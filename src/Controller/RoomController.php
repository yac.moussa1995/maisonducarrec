<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RoomController extends AbstractController
{
    #[Route('/room', name: 'room')]
    public function index(): Response
    {
        return $this->render('room/index.html.twig');
    }

    #[Route('/room/amour', name: 'room_amour')]
    public function amour(): Response
    {
        return $this->render('room/amour.html.twig');
    }

    #[Route('/room/coeur', name: 'room_coeur')]
    public function coeur(): Response
    {
        return $this->render('room/coeur.html.twig');
    }

    #[Route('/room/harmonie', name: 'room_harmonie')]
    public function harmonie(): Response
    {
        return $this->render('room/harmonie.html.twig');
    }

    #[Route('/room/osmose', name: 'room_osmose')]
    public function osmose(): Response
    {
        return $this->render('room/osmose.html.twig');
    }
    #[Route('/room/maison_de_vacances', name: 'room_maison_de_vacances')]
    public function maisonVacance(): Response
    {
        return $this->render('room/maison_de_vacances.html.twig');
    }
}
